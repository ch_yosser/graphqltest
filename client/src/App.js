import React, { Component } from 'react';
// components
import BookList from './components/BookList';
import AddBook from './components/AddBook';
import 'bootstrap/dist/css/bootstrap.min.css';



class App extends Component {
  render() {
    return (
      
            <div id="main">
                <h1>Yosser's Reading List</h1>
                <BookList />
                <AddBook />
            </div>
    );
  }
}

export default App;
