import React, { Component } from 'react';
import { graphql, compose } from 'react-apollo';
import { addAuthorMutation,getAuthorsQuery } from '../queries/queries';

class AddAuthor extends Component {
    constructor(props){
        super(props);
        this.state = {
            name: '',
            age: ''
        };
    }
    displayAuthors(){
        var data = this.props.getAuthorsQuery;
        if(data.loading){
            return( <option disabled>Loading authors</option> );
        } else {
            return data.authors.map(author => {
                return( <li key={ author.id }>{ author.name }</li> );
            });
        }
    }

    submitForm(e){
        e.preventDefault()
        //use the addAuthorMutation
        this.props.addAuthorMutation({
            variables: {
                name: this.state.name,
                age: this.state.age,
            },
        })
        .then((result)=>{
            console.log(result)
        })
        .catch((error)=>{
            console.log("error",error)
        });
    }
    render(){
        return(
            <div id="main">
                    <ul id="book-list">
                            { this.displayAuthors() }
                    </ul>

                    <form id="add-author" onSubmit={ this.submitForm.bind(this) } >
                        <div className="field">
                            <label>Author name:</label>
                            <input type="text" onChange={ (e) => this.setState({ name: e.target.value }) } />
                        </div>
                        <div className="field">
                            <label>Age:</label>
                            <input type="text" onChange={ (e) => this.setState({ age: e.target.value }) } />
                        </div>
                        <button>+</button>
                    </form>
            </div>
        );
    }
}

export default compose (
    graphql(addAuthorMutation, { name: "addAuthorMutation" }),
    graphql(getAuthorsQuery, { name: "getAuthorsQuery",
    options: () => ({
        refetchQueries: [{ query: getAuthorsQuery }],
       
    }),
 })
)(AddAuthor);
