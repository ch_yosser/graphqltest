import React, { Component,useState  } from 'react';
import { withRouter } from 'react-router-dom';
import { graphql,compose } from 'react-apollo';
import { getBooksQuery ,deleteBookMutation} from '../queries/queries';
import { ToastContainer, toast } from 'react-toastify';

// components
import BookDetails from './BookDetails';
import UpdateBook from './updateBook';
import { Button  } from 'react-bootstrap';


class BookList extends Component {
    constructor(props){
        super(props);
        this.state = {
            selected: null
        }
    }

    updateBookPage = (book) => {
        this.props.history.push({
            pathname: '/UpdateBook/'+ book.id,
            state:{'book' : book}
        })
    }

    SuccessNotification = () => toast('Book successfully deleted !', {containerId: 'A'});


    deleteBook = (e,id) => {
        e.preventDefault()
        const { deleteBookMutation } = this.props;
      
        deleteBookMutation({
                    variables: {
                        id : id,
                    }
        }).then((result)=>{
                    this.SuccessNotification()
            }).then(()=>{
                //this.props.history.push('/')
            })
        .catch((error)=>{
                    console.log(error)
                })
           
    }

    displayBooks(){
        
        var data = this.props.data;
        if(data.loading){
            return( <div>Loading books...</div> );
        }
        if (data.error) {
            return <p>{data.error.message}</p>;
        }
        else {
            return data.books.map((book,index) => {
                return(
                    <div key = {index+1}>
                        <li key={ book.id } onClick={ (e) => this.setState({ selected: book.id }) }>{ book.name }</li>
                        <Button onClick ={()=>this.updateBookPage(book)}>Update</Button>
                        <Button onClick ={(e)=>this.deleteBook(e,book)}>Delete</Button>

                    </div>
                    
                );
            })
        }
    }
    render(){

        
        return(
            <div>
                <ul id="book-list">
                    { this.displayBooks() }
                </ul>
                <BookDetails bookId={ this.state.selected } />
                <ToastContainer enableMultiContainer containerId={'A'} position={toast.POSITION.TOP_LEFT} />

            </div>

        );
    }
}



export default compose (
    graphql(getBooksQuery),
    graphql(deleteBookMutation, { name: "deleteBookMutation",
    options : (id) => {
        return {
            variables : {
                id: id
              },
              refetchQueries: [{ query: getBooksQuery }],
        }
        
     
    }
}))(withRouter(BookList))



   