import React, { Component,useState } from 'react';
import PropTypes from 'prop-types';
import { graphql, compose } from 'react-apollo';
import { withRouter } from 'react-router-dom';
import { updateBookMutation,getAuthorsQuery,getBookQuery } from '../queries/queries';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
var selected = ""





class UpdateBook extends Component {
    constructor(props){
        super(props);
        
        this.state = {
            id : this.props.location.state.book.id,
            name:  this.props.location.state.book.name ,
            genre: this.props.location.state.book.genre ,
            authorId: ""

          
        };
    }

    displayAuthors(){
        const {book} = this.props.getBookQuery;
        var data = this.props.getAuthorsQuery;

        if(data.loading){
            return( <option disabled>Loading authors</option> );
        } else {
            return data.authors.map(author => {
                if(book){
                    selected = (book.author.id === author.id) ? true : false;
                }
                
                return( <option key={ author.id } value={author.id} selected ={selected}>{ author.name }</option> );
            });
        }
    }

    SuccessNotification = () => toast('Book successfully updated !', {containerId: 'A'});

    submitForm(e){
        e.preventDefault()
        const {book} = this.props.getBookQuery;
        const { updateBookMutation } = this.props;
        let id,idAuthor
        if(book){
            id = book.id,
            idAuthor = book.author.id
        }
        updateBookMutation({
                    variables: {
                        id : id,
                        name: this.state.name,
                        genre: this.state.genre ,
                        authorId: this.state.authorId === "" ? idAuthor : this.state.authorId
                    }
        }).then((result)=>{
                    this.SuccessNotification()
            }).then(()=>{
                //this.props.history.push('/')
            })
        .catch((error)=>{
                    console.log(error)
                })
           
    }


   
    render(){ 

       return(

       <div>
           <form id="add-book" onSubmit={ this.submitForm.bind(this) } >
                     
                     <div className="field">
                         <label>Book name:</label>
                         <input type="text" onChange={ (e) => this.setState({ name: e.target.value }) } defaultValue={this.state.name}/>
                     </div>
                     <div className="field">
                         <label>Genre:</label>
                         <input type="text" onChange={ (e) => this.setState({ genre: e.target.value }) }  defaultValue={this.state.genre}/>
                     </div>
                     <div className="field">
                         <label>Author:</label>
                         <select onChange={ (e) => this.setState({ authorId: e.target.value }) }>
                             <option>Select author</option>
                             { this.displayAuthors() }
                         </select>
                     </div>
                     <button>+</button>
     
                 </form>
    
                    <ToastContainer enableMultiContainer containerId={'A'} position={toast.POSITION.TOP_LEFT} />
       </div>
            

          
        );
    }
}

UpdateBook.propTypes = {
    updateBookMutation: PropTypes.func.isRequired
  };

export default compose(
    graphql(getAuthorsQuery, { name: "getAuthorsQuery"}),
    graphql(updateBookMutation, { name: "updateBookMutation"}),
    graphql(getBookQuery, {
         name: "getBookQuery",
         options : (props) => {
             return {
                 variables : {
                     id: props.location.state.book.id
                   }
             }
          
         }
       })

)(withRouter(UpdateBook));
