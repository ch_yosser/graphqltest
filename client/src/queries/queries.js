import { gql } from 'apollo-boost';

const getAuthorsQuery = gql`
    {
        authors {
            name
            id
        }
    }
`;

const getBooksQuery = gql`
    {
        books {
            name
            genre
            id
               
        }
    }
`;

const addBookMutation = gql`
    mutation AddBook($name: String!, $genre: String!, $authorId: ID!){
        addBook(name: $name, genre: $genre, authorId: $authorId){
            name
            id
        }
    }
`;

const getBookQuery = gql`
    query GetBook($id: ID){
        book(id: $id) {
            id
            name
            genre
            author {
                id
                name
                age
                books {
                    name
                    id
                }
            }
        }
    }
`;

const addAuthorMutation = gql`
    mutation AddBook($name: String!, $age: Int!){
        addAuthor(name: $name, age: $age){
            name
            age
        }
    }
`;

const updateBookMutation = gql`
    mutation UpdateBook($id : String!, $name: String!, $genre: String!, $authorId : ID!){
        updateBook(id : $id, name: $name, genre: $genre, authorId : $authorId){
            name
            genre
        }
    }
`;

const deleteBookMutation = gql`
    mutation removeBook($id : String!){
        removeBook(id : $id){
            name
            genre
        }
    }
`;



export { getAuthorsQuery, getBooksQuery, addBookMutation, getBookQuery,addAuthorMutation,updateBookMutation,deleteBookMutation };
